(function($, mw) {
    "use strict";
    $(document).ready(function() {
        var css = '<style>#inl_repl_error_box { color: red; border: 1px solid red; padding: 5px; border-radius: 3px; margin-bottom: 5px;background:#ffd7d4;} #repl_inl_preview_box {margin-bottom:5px;} .inl_repl_button, .inl_repl_active {font-size:10pt;margin-top:1px;margin-left: 5px;position:absolute;cursor:pointer;color:#999;font-family:arial,verdana;} .inl_repl_button:hover{color:darkblue;} .inl_repl_active{transform: rotate(45deg);color:red !important;} .inl_repl_control {font-size: 1em !important; margin-right: 5px !important; padding: 6px 12px 6px 12px !important;line-height: 18px;border: 1px solid #ccc;border-radius:2px;} .loading { background-image: url(data:image/gif;base64,R0lGODlhGAAYAKIGAP7+/vv7+/Ly8u/v7+7u7v///////wAAACH/C05FVFNDQVBFMi4wAwEAAAAh+QQFAAAGACwAAAAAGAAYAAADU0hKAvUwvjCWbTIXahfWEdcxDgiJ3Wdu1UiUK5quUzuqoHzBuZ3yGp0HmBEqcEHfjmYkMZXDp8sZgx6JkiayaKWatFhJd1uckrPWcygdXrvUJ1sCACH5BAUAAAYALAAAAAAYABgAAANTSLokUDBKGAZbbupSr8qb1HlgSFnkY55eo67jVZoxM4c189IoubKtmyaH2W2IH+OwJ1NOkK4fVPhk2pwia1GqTXJbUVg3zANTs2asZHwWpX+cQQIAIfkEBQAABgAsAAAAABgAGAAAA1E4tLwCJcoZQ2uP6hLUJdk2dR8IiRL5hSjnXSyqwmc7Y7X84m21MzHRrZET/oA9V8nUGwKLGqcDSpEybcdpM3vVLYNRLrgqpo7K2685hcaqkwkAIfkEBQAABgAsAAAAABgAGAAAA1RYFUP+TgBFq2IQSstxjhNnNR+xiVVQmiF6kdnpLrDWul58o7k9vyUZrvYQ8oigHy24E/UgzQ4yonwWo6kp62dNzrrbr9YoXZEt4HPWjKWk20CmKwEAIfkEBQAABgAsAAAAABgAGAAAA1NYWjH08Amwam0xTstxlhR3OR+xiYv3nahCrmHLlGbcqpqN4hB7vzmZggcSMoA9nYhYMzJ9O2RRyCQoO1KJM9uUVaFYGtjyvY7E5hR3fC6x1WhRAgAh+QQFAAAGACwAAAAAGAAYAAADVFi6FUMwQgGYVU5Kem3WU9UtH8iN2AMSJ1pq7fhuoquaNXrDubyyvc4shCLtIjHZkVhsLIFN5yopfFIvQ2gze/U8CUHsVxDNam2/rjEdZpjVKTYjAQAh+QQFAAAGACwAAAAAGAAYAAADU1i6G0MwQgGYVU5Kem3WU9U1D0hwI1aCaPqxortq7fjSsT1veXfzqcUuUrOZTj3fEBlUmYrKZ/LyCzULVWYzC6Uuu57vNHwcM7KnKxpMOrKdUkUCACH5BAUAAAYALAAAAAAYABgAAANTWLqsMSTKKEC7b856W9aU1S0fyI0OBBInWmrt+G6iq5q1fMN5N0sx346GSq1YPcwQmLwsQ0XHMShcUZXWpud53WajhR8SLO4yytozN016EthGawIAIfkEBQAABgAsAAAAABgAGAAAA1MoUNzOYZBJ53o41ipwltukeI4WEiMJgWGqmu31sptLwrV805zu4T3V6oTyfYi2H4+SPJ6aDyDTiFmKqFEktmSFRrvbhrQoHMbKhbGX+wybc+hxAgAh+QQFAAAGACwAAAAAGAAYAAADVEgqUP7QhaHqajFPW1nWFEd4H7SJBFZKoSisz+mqpcyRq23hdXvTH10HCEKNiBHhBVZQHplOXtC3Q5qoQyh2CYtaIdsn1CidosrFGbO5RSfb35gvAQAh+QQFAAAGACwAAAAAGAAYAAADU0iqAvUwvjCWbTIXahfWEdcRHzhVY2mKnQqynWOeIzPTtZvBl7yiKd8L2BJqeB7jjti7IRlKyZMUDTGTzis0W6Nyc1XIVJfRep1dslSrtoJvG1QCACH5BAUAAAYALAAAAAAYABgAAANSSLoqUDBKGAZbbupSb3ub1HlZGI1XaXIWCa4oo5ox9tJteof1sm+9xoqS0w2DhBmwKPtNkEoN1Cli2o7WD9ajhWWT1NM3+hyHiVzwlkuemIecBAAh+QQFAAAGACwAAAAAGAAYAAADUxhD3CygyEnlcg3WXQLOEUcpH6GJE/mdaHdhLKrCYTs7sXiDrbQ/NdkLF9QNHUXO79FzlUzJyhLam+Y21ujoyLNxgdUv1fu8SsXmbVmbQrN97l4CACH5BAUAAAYALAAAAAAYABgAAANSWBpD/k4ARetq8EnLWdYTV3kfsYkV9p3oUpphW5AZ29KQjeKgfJU6ES8Su6lyxd2x5xvCfLPlIymURqDOpywbtHCpXqvW+OqOxGbKt4kGn8vuBAAh+QQFAAAGACwAAAAAGAAYAAADU1iqMfTwCbBqbTFOy3GWFHc5H7GJi/edaKFmbEuuYeuWZt2+UIzyIBtjptH9iD2jCJgTupBBIdO3hDalVoKykxU4mddddzvCUS3gc7mkTo2xZmUCACH5BAUAAAYALAAAAAAYABgAAANTWLoaQzBCAZhtT0Z6rdNb1S0fSHAjZp5iWoKom8Ht+GqxPeP1uEs52yrYuYVSpN+kV1SykCoatGBcTqtPKJZ42TK7TsLXExZcy+PkMB2VIrHZQgIAIfkEBQAABgAsAAAAABgAGAAAA1RYuhxDMEIBmFVOSnpt1lPVLR/IjdgDEidaau34bqKrmrV8w3k3RzHfjoZaDIE934qVvPyYxdQqKJw2PUdo9El1ZrtYa7TAvTayBDMJLRg/tbYlJwEAIfkEBQAABgAsAAAAABgAGAAAA1IItdwbg8gphbsFUioUZtpWeV8WiURXPqeorqFLfvH2ljU3Y/l00y3b7tIbrUyo1NBRVB6bv09Qd8wko7yp8al1clFYYjfMHC/L4HOjSF6bq80EACH5BAUAAAYALAAAAAAYABgAAANTSALV/i0MQqtiMEtrcX4bRwkfFIpL6Zxcqhas5apxNZf16OGTeL2wHmr3yf1exltR2CJqmDKnCWqTgqg6YAF7RPq6NKxy6Rs/y9YrWpszT9fAWgIAOw==); }</style>';
        var control = '<div id="inl_repl_dialog" style="margin-top:3px;max-width:600px;min-width:450px;width:50%;"><textarea style="font-family: courier new; height: 56px;padding:3px;width: 100%;" class="mw-ui-input" id="inl_repl_text"></textarea><div style="padding-top:5px;padding-bottom:5px;display:flex;"><div style="flex-shrink: 0;"><input type="button" value="Speichern" id="inl_repl_save" class="mw-ui-button mw-ui-progressive inl_repl_control"><input type="button" class="mw-ui-button inl_repl_control" value="Vorschau" id="inl_repl_preview"><input type="button" class="mw-ui-button inl_repl_control" value="&#x2759;&#10094;" id="repl_inl_left_start" style="min-width: 0px;letter-spacing:-4px;padding-left: 8px !important;padding-right: 13px !important;"><input type="button" class="mw-ui-button inl_repl_control" value="&#10094;" id="repl_inl_left" style="min-width: 0px;"><input type="button" class="mw-ui-button inl_repl_control" value="&#10095;" style="min-width: 0px;" id="repl_inl_right"></div><div style="flex-grow: 1;"><input type="text" value="+AW" class="mw-ui-input inl_repl_control" style="color: #555;width: 100%;box-sizing: border-box;" id="inl_repl_summary"></div></div></div>';

        function plain_text(obj) {
            var exclude = ['DL', 'DD'];
            return obj.contents().filter(function() {
                return exclude.indexOf(this.nodeName) === -1;
            }).text();
        }

        function add_focus_action() {
            var placeholder = '+AW';
            $("#inl_repl_summary", $dialog).focus(function() {
                if ($(this).val() === placeholder) {
                    $(this).val('');
                }
            });
            $("#inl_repl_summary", $dialog).blur(function() {
                if ($(this).val() === '') {
                    $(this).val(placeholder);
                }
            });
        }

        function add_preview_action() {
            $('#inl_repl_preview', $dialog).on('click', function() {
                toggle_interface(true);
                api.post({
                    action: 'parse',
                    text: get_textarea(),
                    contentmodel: 'wikitext'
                }).done(function(data) {
                    toggle_interface(false);
                    $('#inl_repl_error_box, #repl_inl_preview_box', $dialog).remove();
                    $('#inl_repl_text', $dialog).before('<div id="repl_inl_preview_box">' + data.parse.text['*'] + '</div>').focus();
                    $('#repl_inl_preview_box', $dialog).hide().fadeIn(200);
                });
            });
        }

        function add_drop_action() {
            $("#inl_repl_text", $dialog).bind("drop", function(e) {
                var user_re = new RegExp('https?:\/\/de\.wikipedia\.org\/wiki\/(Benutzeri?n?|Benutzeri?n?)(?:[_\s]Diskussion)?:(.+)');
                var drop_value = e.originalEvent.dataTransfer.getData("text") || e.originalEvent.dataTransfer.getData("text/plain");
                var match = drop_value.match(user_re);
                if (match) {
                    $(this).val($(this).val() + '[[' + match[1] + ':' + match[2] + '|' + match[2] + ']]').focus();
                } else {
                    $(this).val($(this).val() + drop_value).focus();
                }
                return false;
            });
        }

        function remove_elements(obj) {
            var current = $(obj).parent();
            var headline = false;
            var rm;
            while ($(current).is('dd,dl,p,pre,div:not(.mw-parser-output)')) {
                if (headline === false && $(current).prev('h1,h2,h3,h4,h5,h6').length) {
                    headline = $(current).prev('h1,h2,h3,h4,h5,h6');
                }
                if ($(current).children('h1,h2,h3,h4,h5,h6').length > 0) {
                    break;
                } else if ($(current).prev('dd,dl,p,pre,div').length) {
                    rm = current;
                    current = $(current).prev();
                    $(rm).remove();
                } else if ($(current).next('dd,dl,p,pre,div').length) {
                    rm = current;
                    current = $(current).next();
                    $(rm).remove();
                } else {
                    rm = current;
                    current = $(current).parent();
                    $(rm).remove();
                }
            }
            return headline;
        }

        function detect_last_post(obj) {
            var last_post = false;
            var current = obj;
            while ($(current).is('span,dd,dl,p,pre,div:not(.mw-parser-output)')) {
                if ($(current).next('dl,dd').length > 0) {
                    last_post = false;
                    break;
                } else if ($(current).parent('div').is('.mw-parser-output') && $(current).next().length === 0 && $(current).children('h1,h2,h3,h4,h5,h6').length === 0) {
                    last_post = true;
                    break;
                } else if ($(current).next('h1,h2,h3,h4,h5,h6').length === 0) {
                    current = $(current).parent('dl,dd,p,pre,div');
                } else {
                    var h_set = ['H1', 'H2', 'H3', 'H4', 'H5', 'H6'];
                    var prev_h = $(current).prevAll('h1,h2,h3,h4,h5,h6').first().prop('tagName');
                    var next_h = $(current).next('h1,h2,h3,h4,h5,h6').prop('tagName');
                    if (h_set.indexOf(prev_h) < h_set.indexOf(next_h)) {
                        last_post = false;
                    } else {
                        last_post = true;
                    }
                    break;
                }
            }
            return last_post;
        }

        function toggle_interface(state) {
            if (state === true) {
                $('#inl_repl_text', $dialog).addClass('loading');
            } else {
                $('#inl_repl_text', $dialog).removeClass('loading');
            }
            $('.inl_repl_control', $dialog).prop("disabled", state);
            $('#inl_repl_text', $dialog).prop("disabled", state);
            if (indent === 0) {
                $('#repl_inl_left, #repl_inl_left_start', $dialog).prop("disabled", true);
            }
            if (margin_steps === 10) {
                $('#repl_inl_right', $dialog).prop("disabled", true);
            }
        }

        function get_textarea() {
            var textarea = $('#inl_repl_text', $dialog).val();
            var space = '';
            if (indent === 0) {
                textarea = textarea.trimEnd();
            } else {
                textarea = textarea.trim();
            }
            if (!textarea.includes('~~~~')) {
                if (textarea.length > 0) {
                    space = ' ';
                }
                textarea = textarea + space + '--~~~~';
            }
            return textarea;
        }

        function escapeRegex(string) {
            return string.replace(/[-\/\\^$*+?.()x|[\]{}]/g, '\\$&');
        }

        function get_signatures(obj, re) {
            var inline_re = new RegExp(re, 'g');
            var signature = [];
            var match;
            var deep = false;
            var current = $(obj).parent();
            while ($(current).is('dd,dl,p,pre,div:not(.mw-parser-output)')) {
                if ($(current).children('h1,h2,h3,h4,h5,h6').length > 0) {
                    break;
                }
                if (deep === true) {
                    match = $(current).text().match(inline_re);
                } else {
                    match = plain_text(current).match(inline_re);
                }
                if (match) {
                    signature = signature.concat(match);
                }
                if ($(current).prev('dd,dl,p,pre,div').length) {
                    current = $(current).prev();
                    deep = true;
                } else {
                    current = $(current).parent();
                    deep = false;
                }
            }
            return signature;
        }

        function insert_links() {
            var i, x;
            var elem = document.getElementById('mw-content-text').getElementsByClassName('mw-parser-output');
            elem = elem[0].querySelectorAll('p,dd,div,pre');
            for (i = 0; i < elem.length; ++i) {
                var match = false;
                for (x = 0; x < elem[i].childNodes.length; x++) {
                    if (elem[i].childNodes[x].nodeType === 3 || (["SMALL", "CODE"].includes(elem[i].childNodes[x].nodeName))) {
                        if (re.test(elem[i].childNodes[x].textContent)) {
                            match = true;
                            break;
                        }
                    }
                }
                if (match === true) {
                    if (elem[i].getElementsByTagName('dl').length) {
                        $(elem[i]).children('dl').first().before(box);
                    } else if (['right', 'left'].includes(elem[i].style.float)) {
                        continue;
                    } else {
                        $(elem[i]).html($(elem[i]).html().trim() + box);
                    }
                }
            }
        }

        function show_error(error, origin) {
            toggle_interface(false);
            $('#inl_repl_error_box', $dialog).remove();
            $('#inl_repl_text', $dialog).before('<div id="inl_repl_error_box">[' + origin + '] ERROR: ' + error + '</div>');
            $('#inl_repl_error_box', $dialog).hide().fadeIn(200);
            $('#inl_repl_text', $dialog).focus();
        }

        function add_move_action() {
            $('#repl_inl_left', $dialog).on('click', function() {
                if (indent - 1 >= 0) {
                    margin_steps = margin_steps - 1;
                    indent = indent - 1;
                    $dialog.css('margin-left', margin_steps * 1.6 + 'em');
                }
            });
            $('#repl_inl_right', $dialog).on('click', function() {
                if (margin_steps + 1 <= 10) {
                    margin_steps = margin_steps + 1;
                    indent = indent + 1;
                    $dialog.css('margin-left', margin_steps * 1.6 + 'em');
                }
            });
            $('#repl_inl_left_start', $dialog).on('click', function() {
                margin_steps = margin_steps - indent;
                indent = 0;
                $dialog.css('margin-left', margin_steps * 1.6 + 'em');
            });
            $('#repl_inl_left,#repl_inl_right,#repl_inl_left_start', $dialog).on('click', function() {
                if (indent === 0) {
                    $('#repl_inl_left, #repl_inl_left_start', $dialog).prop("disabled", true);
                } else {
                    $('#repl_inl_left, #repl_inl_left_start', $dialog).prop("disabled", false);
                }
                if (margin_steps === 10) {
                    $('#repl_inl_right', $dialog).prop("disabled", true);
                } else {
                    $('#repl_inl_right', $dialog).prop("disabled", false);
                }
                $('#inl_repl_text', $dialog).focus();
            });
        }

        function check_namespace(permitted) {
            if (permitted.includes(mw.config.get('wgNamespaceNumber'))) {
                return true;
            } else {
                return false;
            }
        }
        var permitted_namespace = [1, 3, 4, 5, 7, 9, 11, 13, 15, 100, 101, 829, 2301, 2303];
        if (check_namespace(permitted_namespace) === false) {
            return false;
        }
        var box = '<span class="inl_repl_button"><b>&#10010;</b></span>';
        var re = /[0-9]{2}:[0-9]{2},\s[0-9]{1,2}\.\s[A-Z][a-zä]{2}\.?\s[0-9]{4}\s\(CES?T\)/;
        var api = new mw.Api();
        var margin_steps;
        var indent;
        var new_line_required;
        var $root = $('#mw-content-text .mw-parser-output');
        var $dialog = null;
        $('body').append(css);
        insert_links();
        $(document).on('click', '.inl_repl_button', function() {
            var this_button = this;
            var section_re = /title=([^&]+)&.+&section=([0-9T-]+)/;
            var headline = $(this).parents().prevAll('h1,h2,h3,h4,h5,h6').first();
            var section_url = $(headline).children('span.mw-editsection').children('a').first().attr('href');
            var section_match = section_url.match(section_re);
            var page_title = section_match[1];
            var section_id = section_match[2];
            margin_steps = 0;
            indent = 0;
            var signature = get_signatures(this, re);
            var last_post = detect_last_post(this);
            $('.inl_repl_active', $root).addClass('inl_repl_button').removeClass('inl_repl_active');
            $(this).addClass('inl_repl_active').removeClass('inl_repl_button');
            $('#inl_repl_dialog', $root).parent().parent().remove();
            if ($(this).parent().is('div,pre')) {
                $(this).parent().after('<dl><dd style="margin-top: 10px;">' + control + '</dd></dl>');
            } else if ($(this).parent().children('dl').length || $(this).parent().next('dl').length) {
                $(this).after('<dl><dd><dl><dd>' + control + '</dd></dl></dd></dl>');
            } else {
                $(this).after('<dl><dd>' + control + '</dd></dl>');
            }
            $dialog = $('#inl_repl_dialog', $root);
            $dialog.hide().slideDown(100);
            $('#inl_repl_text', $dialog).focus();
            indent = $dialog.parents('dl').length;
            if ($(this).parent().is('p')) {
                new_line_required = true;
            }
            add_drop_action();
            add_preview_action();
            add_focus_action();
            add_move_action();
            var indent_match = new RegExp('(:+)[^\n]+', 'g');
            var sign_match = new RegExp(escapeRegex(signature[0]) + '[^\n]*', 'g');
            var sign_count = signature.filter(x => x === signature[0]).length;
            $('#inl_repl_save', $dialog).on('click', function() {
                toggle_interface(true);
                api.get({
                    action: 'query',
                    meta: 'tokens',
                    titles: page_title,
                    formatversion: 2,
                    curtimestamp: 1,
                    prop: 'revisions',
                    format: 'json'
                }).done(function(data) {
                    var base_time = data.query.pages[0].revisions[0].timestamp;
                    var current_timestamp = data.curtimestamp;
                    var token = data.query.tokens.csrftoken;
                    api.get({
                        action: 'parse',
                        page: page_title,
                        prop: 'wikitext|sections',
                        section: section_id
                    }).done(function(data) {
                        var wiki_text = data.parse.wikitext['*'];
                        var section_text = data.parse.sections[0].line;
                        var result_set = [];
                        var match;
                        while ((match = sign_match.exec(wiki_text)) !== null) {
                            result_set.push(sign_match.lastIndex);
                        }
                        result_set.length = sign_count;
                        var last_index = result_set.pop();
                        var first_part = wiki_text.slice(0, last_index);
                        var second_part_temp = wiki_text.slice(last_index, wiki_text.length);
                        var part_match;
                        var part_match_result = [];
                        while ((part_match = indent_match.exec(second_part_temp)) !== null) {
                            part_match_result.push(part_match[1]);
                        }
                        if (part_match_result.length > 0 && last_post === true && margin_steps === 0) {
                            indent = part_match_result.pop().length + 1;
                            first_part = wiki_text;
                        }
                        var second_part = '\n';
                        var text_input = get_textarea();
                        if (indent === 0 && new_line_required === true) {
                            second_part += '\n';
                        }
                        second_part += ':'.repeat(indent) + text_input;
                        if (wiki_text.length > last_index && last_post === false) {
                            second_part = second_part + second_part_temp;
                        }
                        var content = first_part + second_part;
                        var summary = '/* ' + section_text + ' */ ' + $('#inl_repl_summary', $dialog).val();
                        summary = summary.trim() + " ([[Benutzer:FNDE/InstantReply|IR]])";
                        api.post({
                            action: 'edit',
                            format: 'json',
                            summary: summary,
                            text: content,
                            title: page_title,
                            basetimestamp: base_time,
                            starttimestamp: current_timestamp,
                            token: token,
                            section: section_id
                        }).done(function() {
                            api.get({
                                action: 'parse',
                                disableeditsection: true,
                                page: page_title,
                                prop: 'text',
                                section: section_id
                            }).done(function(data) {
                                var wiki_text = $.parseHTML(data.parse.text['*']);
                                var headline_obj = remove_elements(this_button);
                                $(headline_obj).after(wiki_text);
                                var parser_output = $(headline_obj).next('.mw-parser-output');
                                $(parser_output).children('h1,h2,h3,h4,h5,h6').first().remove();
                                $(parser_output).children('h1,h2,h3,h4,h5,h6').first().nextAll().andSelf().remove();
                                $(parser_output).children().first().unwrap();
                                $('.inl_repl_button').remove();
                                insert_links();
                            }).fail(function(e) {
                                show_error(e, 'PARSE');
                            });
                        }).fail(function(e) {
                            show_error(e, 'EDIT');
                        });
                    }).fail(function(e) {
                        show_error(e, 'TEXT');
                    });
                }).fail(function(e) {
                    show_error(e, 'TOKEN');
                });
            });
        });
        $(document).on('click', '.inl_repl_active', function() {
            $(this).addClass('inl_repl_button').removeClass('inl_repl_active');
            $('#inl_repl_dialog', $root).slideUp(100, function() {
                $(this).parent().parent().remove();
            });
        });
    });
})(jQuery, mediaWiki);

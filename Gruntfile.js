module.exports = function(grunt) {

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks("grunt-jsbeautifier");
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-remove-comments');
    grunt.loadNpmTasks('grunt-strip-code');

    grunt.initConfig({

        jshint: {
            prod: {
                src: ['src/*.js'],
                options: {
                    esversion: 6,
                    undef: true,
                    curly: true,
                    eqeqeq: true,
                    strict: true,
                    globals: {
                        document: true,
                        window: true,
                        console: true,
                        jQuery: true,
                        mediaWiki: true
                    }
                }
            },
            dist: {
                src: ['dist/*.js'],
                options: {
                    esversion: 6,
                    undef: true,
                    curly: true,
                    eqeqeq: true,
                    strict: true,
                    unused: true, // only in dist
                    globals: {
                        document: true,
                        window: true,
                        console: true,
                        jQuery: true,
                        mediaWiki: true
                    }
                }
            }
        },

        strip_code: {
            options: {
                blocks: [{
                    start_block: "/* DEV BLOCK */",
                    end_block: "/* DEV BLOCK END */"
                }]
            },
            prod: {
                src: 'dist/*.js'
            }
        },

        remove_comments: {
            js: {
                src: 'dist/reply.js',
                expand: true
            },
        },

        copy: {
            prod: {
                nonull: true,
                src: 'src/reply.js',
                dest: 'dist/reply.js',
            },
        },

        jsbeautifier: {
            prod: {
                src: ["dist/reply.js"],
                options: {
                    js: {
                        indentSize: 4,
                        preserve_newlines: false
                    }
                }
            },
            control: {
                src: ["Gruntfile.js"],
                options: {
                    js: {
                        indentSize: 4
                    }
                }
            },
        }

    });

    grunt.registerTask('dist', ['jshint:prod', 'copy:prod', 'strip_code', 'remove_comments:js', 'jsbeautifier:prod', 'jsbeautifier:control', 'jshint:dist']);

};

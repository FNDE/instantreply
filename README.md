## InstantReply
Simplify the process of answering to contributions in discussions at MediaWiki, as it was once intended with Flow.

-> [Project Page @ Wikipedia](https://de.wikipedia.org/wiki/Benutzer:FNDE/InstantReply)
